<center><h1>SOLID PRINCIPLES</h1></center>
 <ul>
  <li><h2>SRP - Single Responsibility Principle</h2></li>
  <li><h2>OCP - Open Closed Principle</h2></li>
  <li><h2>LSP - LISKOV Substitution Principle</h2></li>
  <li><h2>ISP - Interface Segregation Principle</h2></li>
  <li><h2>DIP - Dependency Inversion Principle</h2></li>
 </ul>
 <br>
 <h1><b>SRP - Single Responsibility Principle</h1>
<h2>Class/Package/Component/API should have only one and only reason for change.</h2>
<h2><b>Example</b></h2>
<h2 style="margin-left :70px;">Task</h2>
<h3 style="margin-left :50px;">+dnloadFile(String Location)</h3>
<h3 style="margin-left :50px;">+parseTheFile(File file)</h3>
<h3 style="margin-left :50px;">+persistTheData(Data data)</h3><br>
<h2 style="margin-left :70px;">Report</h2>
<h3 style="margin-left :50px;">+printReport()</h3>
<h3 style="margin-left :50px;">+getReportData()</h3>
<h3 style="margin-left :50px;">+formatReport</h3><br>

<h1><b>OCP -Open Closed Principle</b></h1>
<h2>Any application should be open for extension and closed for modification.</h2>
<h2><b>Example</b></h2>
<h3>// Open-Close Principle - Bad example </h3>
<h3>class GraphicEditor{</h3>
<h3 style="margin-left:40px">public void drawShape(Shape s){</h3>
<h3 style="margin-left:60px">if(s.m_type == 1)</h3>
<h3 style="margin-left:70px">drawRectangle(s);</h3>
<h3 style="margin-left:60px">else if(s.m_type == 2)</h3>
<h3 style="margin-left:70px">drawCircle(s);<br>}</h3>
<h3 style="margin-left:40px">public void drawCircle(Circle r)<br>&nbsp&nbsp{....}</h3>
<h3 style="margin-left:40px">public void drawRectangle(Rectangle r)<br>&nbsp&nbsp{....}</h3>
<h3>}</h3>



<br><br>
<h3>class Shape{</h3>
<h3 style="margin-left:40px">int m_type;</h3>
<h3 style="margin-left:40px">}</h3>
<h3>class Rectangle extends Shape{</h3>
<h3 style="margin-left:50px">Rectangle(){</h3>
<h3 style="margin-left:70px">super.m_type =1;<br>}</h3>
<h3 style="margin-left:40px">}</h3>
<h3>class Circle extends Shape{</h3>
<h3 style="margin-left:50px">Circle(){</h3>
<h3 style="margin-left:70px">super.m_type =2;<br>}</h3>
<h3 style="margin-left:40px">}</h3>

<br>
<h1>LSP - LISKOV Substitution Principle</h1>
<h2>Child class should be able to substitute Parent Class anytime with same behaviour</h2>
<h2><b>Good Example-</b></h2>

<center><div style="width:300px;border : solid black ">
   <h2><b>Vehicle<hr></b></h2>
   <h3>+getSpeed():int</h3>
    <h3>+getCubicCapacity():int</h3>
</div></center>


<h2>Here Car Extends Vehicle</h2>
<center><div style="width:300px;border : solid black ">
   <h2><b>Car extends Vehicle<hr></b></h2>
   <h3>+getSpeed():int</h3>
    <h3>+getCubicCapacity():int</h3>
    <h3>+isHatchBack():boolean</h3>
</div></center>

<h2>Here Bus Extends Vehicle</h2>

<center><div style="width:300px;border : solid black ">
   <h2><b>Bus extends Vehicle<hr></b></h2>
   <h3>+getSpeed():int</h3>
    <h3>+getCubicCapacity():int</h3>
    <h3>+getEmergencyExitLoc():String</h3>
</div></center>
<br>
<h1>ISP - Interface Segregation Principle</h1>
<h2>Client should not be forced to implement interface even if it is not required.</h2>
<h2>Create as small interface as possible.</h2>
<h2><b>Example-</b></h2>

<h3>Interface Aniaml<br>{<br>void feed();<br>}</h3>
<h3>Dog implements Animal<br>{<br>void feed();<br>}</h3>
<h3>Tiger implements Aniaml<br>{<br>void feed();<br>}</h3>
<br><br>
<h2>If we add new method in Animal-</h2><br>
<h3>Interface Aniaml<br>{<br>void feed();<br><l style="color:red">void play();<br></l>}</h3>
<h3>Dog implements Aniaml<br>{<br>void feed();<br>void play();<br>}</h3>
<h3>Tiger implements Animal<br>{<br>void feed();<br><l style="color:red">void play();<br></l>}</h3>

<h1>DIP - Dependency Inversion Principle</h1>
<h2>Depend upon Interfaces or abstract Classes rather than Concrete classes</h2>






<center>end</center>
